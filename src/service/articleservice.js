import instance from '../config/axiosconf'

export default{
    login(data){
         return instance.post("/user/login", data)
    },
    signup(data){
        return instance.post("/user/signup", data)
    },
    getArticle(id){
        return instance.get(`/article/${id}`)
    },
    getArticles(){
        return instance.get("/article/getAll")
    },
    getAllMine(){
        return instance.get('/article/getAllMine/')
    },
    updateArticle(article){
        return instance.put('/article', article)
    },
    createArticle(article){
        return instance.post('/article', article)
    }
}
