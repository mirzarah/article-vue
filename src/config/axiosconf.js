import axios from 'axios'
import config from '../config/config'
import { notify } from "@kyvg/vue3-notification";
import  { store }  from '../config/store'


const instance = axios.create({
    baseURL: config.api_url
});


instance.interceptors.request.use(function (config) {

    if(store.state.token){
        instance.defaults.headers.common['Authorization'] = 'Bearer '+store.state.token
    }
    else{
        delete instance.defaults.headers.common['Authorization']
    }

    return config;
  }, function (error) {
    return Promise.reject(error);
  });

instance.interceptors.response.use((response) => response, function(error) {   

    let data = error.response.data

    let msgs = []
    if (
        typeof data === 'object' &&
        !Array.isArray(data) &&
        data !== null
    ) {
        for(let msg in data){
            msgs.push(data[msg])
        }
    }
    else{
        msgs.push(data)
    }

    let flatMsg = 'There is a problem!'

    if(msgs.length > 0){
        flatMsg = msgs.join(" ")
    }

    if(error.response.status == 401){
        flatMsg = 'Unauthorized. Please login!'
    }

    notify({
        type:'error',
        title: 'Error',
        text: flatMsg
      })

    throw error;
  })



export default instance
