import { createStore } from 'vuex'
import axiosinstance from '../config/axiosconf'

 const store = createStore({
  state () {
    return {
      count: 0,
      user: null,
      isLogged: localStorage.getItem('isLogged') || false,
      token: localStorage.getItem('token') || '',
    }
  },
  getters: {

    },
    mutations: {
        auth_success(state, payload) {
            state.isLogged = true
            state.token = payload.token
            state.user = payload.user
          },
          logout(state){
            state.isLogged = false
            state.token = null
            state.user = null
          },
          restore_user(state, payload){
            state.user = payload
          }
    },
    actions: {
        login({commit}, user) {
            return new Promise((resolve) => {

              const token = user.token
              const userLoged = user
      
              localStorage.setItem('token', token)
              localStorage.setItem('isLogged', true)
              localStorage.setItem('user', JSON.stringify(user))

              axiosinstance.defaults.headers.common['Authorization'] ='Bearer '+ token
              commit('auth_success', { token: token, user: userLoged })
              resolve(userLoged)
              
            })
          },
          logout({commit}){
            return new Promise((resolve) => {
              commit('logout')
              localStorage.removeItem('token')
              localStorage.removeItem('isLogged')
              localStorage.removeItem('user')

              delete axiosinstance.defaults.headers.common['Authorization']
              resolve()
            })
          },
          restoreUser({commit}){
            let userString = JSON.parse(localStorage.getItem('user'))
            commit("restore_user", userString)
          },
    },
    modules: {

    }
})

export default function(){
  return store
}

export { store }