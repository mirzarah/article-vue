import { createRouter, createWebHashHistory } from 'vue-router'
import  { store }  from '../config/store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Article.vue')
  },
  {
    path: '/article',
    name: 'Article',
    component: () => import('../views/Article.vue')
  },
  {
    path: '/article/view/:id',
    name: 'ArticleView',
    component: () => import('../components/ArticleView.vue')
  },
  {
    path: '/article/edit/:id',
    name: 'ArticleEdit',
    component: () => import('../components/ArticleEdit.vue')
  },
  {
    path: '/article/create',
    name: 'ArticleCreate',
    component: () => import('../components/ArticleCreate.vue')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import('../views/Signup.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {

  let authorizedRoutes = ['ArticleCreate', 'ArticleEdit']
  let isAuthorizedRoute = authorizedRoutes.filter(x=>x == to.name)

  if(isAuthorizedRoute.length>0 && to.name !='Login' && !store.state.isLogged) next({name: 'Login'})
  else 
  next()
})

export default router
