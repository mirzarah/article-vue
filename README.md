# proj

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Optional

Please check your backend api url if its diffrent than from one currently saved in file config/config.js, you may need to update it
    api_url:"https://localhost:44349/api"

###

Node v14.17.6

### TODO
```
Restrict atricle editing button on article view component
Logout dialog confirmation
UI improvement

```
